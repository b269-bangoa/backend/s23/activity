//console.log("Hi");



let trainer = {
    name: "Ash Ketchun", 
    age: 10, 
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"], 
    friends: ["May", "Rocky", "Brock", "Missy"],
    talk: function(chosenPokemon) {
        chosenPokemon = prompt("Choose your POKEMON: ");
        if(this.pokemon.includes(chosenPokemon)) {
            return console.log(chosenPokemon + (" I choose you!"));
        } else {
            return alert("Invalid Pokemon, please choose from your pokemons!");
        }
    }
};

console.log(trainer);
console.log("Result of got notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method:");
//trainer.talk();


// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    this.faint = function() {
        console.log(this.name + " fainted");  
    }
    this.tackle = function(target) {
       // target.health = target.health - this.attack;

        /*for(health = target.health; health >= 0; health --) {

            if (health > 0) {
                console.log(target.name + "'s' health is now reduced to " + health);
                health = health - this.attack;
            } if (health <= 0) {
                console.log(target.name + "'s' health is now reduced to " + health);
                target.faint();
            }
            
        }*/
        console.log(this.name + " tackled " + target.name);

        if (target.health > 0) {
            console.log(target.name + "'s' health is now reduced to " + target.health);
            target.health = target.health - this.attack;
        } else {
            console.log(target.name + "'s' health is now reduced to " + target.health);
            target.faint();
        }

    }
    
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon('Rattata', 8);
let geodude = new Pokemon("Geodue", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu,rattata,geodude,mewtwo);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects

pikachu.tackle(mewtwo);
geodude.tackle(pikachu);
mewtwo.tackle(pikachu);
